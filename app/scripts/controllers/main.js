'use strict';

/**
 * @ngdoc function
 * @name chartappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the chartappApp
 */
angular.module('chartappApp')
  .controller('MainCtrl', [ 'DataService', '$scope', function (DataService, $scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ],
    $scope.chartObject = {
       type: 'BarChart',
       data: {"cols": [
        {id: "month", label: "Month", type: "string"},
        {id: "laptop-id", label: "Laptop", type: "number"},
    ], "rows": [
        {c: [
            {v: "UPMC Shadyside"},
            {v: 14}
        ]},
        {c: [
            {v: "UPMC Mercy"},
            {v: 2}
        ]},
        {c: [
            {v: "UPMC East"},
            {v: 6}
        ]}
    ]},
    options: {
        "title": "Sales per month",
        "isStacked": "true",
        "fill": 20,
        "displayExactValues": true,
        "vAxis": {
            "title": "Sales unit", "gridlines": {"count": 6}
        },
        "hAxis": {
            "title": "Date"
        }
    }
    },
    $scope.data = DataService.getData();
    
    $scope.options = {
        series: [
            {dataset: 'dataset0', key: 'aaa', label: 'zzz', type: [ 'line' ]}
        ]
    }
}])
.factory('DataService', ['$http', function($http) {
    return {
        getData: function() {
            //return $http.get('https://localhost:44300/api/interviewsperfacilities')
            return $http.get('data/sample_data.json')
                .then(function(result) {
                    console.log(result.data);
                    return result.data;
                });
        }
    }
}]);