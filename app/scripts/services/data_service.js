'use strict';

angular.module('chartappApp')
    .factory('DataService', ['$http', function($http) {
        return {
            getData: function() {
                //since $http.get returns a promise,
                //and promise.then() also returns a promise
                //that resolves to whatever value is returned in it's 
                //callback argument, we can return that.
                return $http.get('https://localhost:44300/api/interviewsperfacilities')
                    .then(function(result) {
                        return result.data;
                    });
            }
        }
}]);